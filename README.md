# Readme
Documentação instrutiva para configuração de ambiente e 
testagem do código.
## Instruções
### Pré-requisitos
As atividades deste projeto foram realizadas utilizando as seguintes
tecnologias, em suas reespectivas versões:

* Python 3.8.10
* Docker version 20.10.8
* docker-compose version 1.29.2

Após certificar-se de ter instalado corretamente os itens descritos,
siga a o passo-a-passo abaixo.

### Instalação
A fim de facilitar a portabilidade de ambiente, 
uma imagem docker foi gerada. Para aplica-la em seu ambiente 
local basta seguir o passo-a-passo:

#### Preparando o ambiente
1. Navegue até o diretório o qual deseja-se alocar os 
arquivos.
2. Clone o repositório.
3. Acesse a pasta criada.
4. Via terminal, utilize o comando *echo* 
para declarar variáveis de ambientes necessárias no 
arquivo **.env**, como mostra o exemplo abaixo:

``` bash
echo -e "AIRFLOW_UID=$(id -u)\nAIRFLOW_GID=0" > .env
```

#### Criando a aplicação
É necessário inicializar os containers, para isso, execute o comando:

```bash
docker-compose up airflow-init
```

Após a execução completa do processo, uma mensagem, similar à 
citada abaixo, deve aparecer:

```bash
airflow-init_1       | Upgrades done
airflow-init_1       | Admin user airflow created
airflow-init_1       | 2.1.2
start_airflow-init_1 exited with code 0
```

#### Inicializando a aplicação
Para inicializar o **Airflow**, basta executar o seguinte comando:
```bash
docker-compose up
```

A aplicação criará uma rota na porta:
* http://localhost:8080/

#### Credenciais
* Login = airflow
* Password = airflow

