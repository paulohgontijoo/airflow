# Imports
import datetime as dt
import os
from os import listdir
from os.path import isfile, join
import requests
from bs4 import BeautifulSoup

from airflow import DAG
from airflow.operators.python import PythonOperator
from airflow.operators.postgres_operator import PostgresOperator
from airflow.hooks.postgres_hook import PostgresHook


# Classes de constantes
class DataPath:
    RAW = '/opt/airflow/data/raw'


class Url:
    CVM_INFORMES = 'http://dados.cvm.gov.br/dataset/fi-doc-inf_diario'


# Faz requisição com a url
def get_page(url):
    html = requests.get(url).content
    soup = BeautifulSoup(html)
    return soup


# Raspagem dos dados para o obter links de downloads
def scrap_link() -> list:
    soup = get_page(Url.CVM_INFORMES)
    content = soup.find_all(class_='resource-url-analytics')
    list_links = list()
    for i in range(len(content)):
        link = str(content[i]).split()[2].split('href="')[1][:-1]
        list_links.append(link)
    return list_links


# Define o path a ser salvo o arquivo original
def get_file_name(constant, link):
    return str(constant + '/' + link.split('/')[-1])


# Função de coleta dos dados
def download_data():
    # Requisita os dados
    # Salva os arquivos na pasta /data/raw
    for i in range(len(scrap_link())):
        link = scrap_link()[i]
        req = requests.get(link)
        url_content = req.content
        pwd = os.getcwd()
        split = pwd.split("/")
        path_pwd = "/".join(split[:-2])
        path = path_pwd + get_file_name(DataPath.RAW, link)
        print('Saving file at: ', path)
        csv_file = open(path, 'wb', )
        csv_file.write(url_content)
        csv_file.close()

# Obtem path do arquivo mais recente
def get_latest_version():
    onlyfiles = [f for f in listdir(DataPath.RAW) if isfile(join(DataPath.RAW, f))]
    newest = 0
    for each in onlyfiles:
        split = onlyfiles[0].split('_')[-1]
        date = int(split.split('.csv')[0])
        if date > newest:
            newest = date
            name = each
    return str(DataPath.RAW + '/' + name)


def csv_to_postgres():
    # Cria conexão com o banco de dados
    pg_hook = PostgresHook(postgres_conn_id='Postgres')
    get_postgres_conn = PostgresHook(postgres_conn_id='Postgres').get_conn()
    curr = get_postgres_conn.cursor("cursor")

    # Carrega o arquivo na tabela
    with open(get_latest_version(), 'r') as f:
        next(f)
        curr.copy_from(f, 'cvm', sep=';')
        get_postgres_conn.commit()


# Declaração de argumentos da DAG
default_args = {
    'owner': 'airflow',
    'start_date': dt.datetime(2018, 10, 1, 10, 00, 00),
    'concurrency': 1,
    'retries': 0,
    'email': ['paulohgontijoo@gmail.com'],
    'email_on_failure': True,
}

# Declaração da DAG
with DAG(dag_id='get_cvm_data',
         catchup=False,
         default_args=default_args,
         schedule_interval='0 10 * * *',
         ) as dag:
    # Operador responsável por coletar os dados
    opr_get_cvm = PythonOperator(task_id='download_data',
                                 python_callable=download_data,
                                 provide_context=True)

    # Operador responsável por criar a tabela no banco de dados
    opr_create_table = PostgresOperator(task_id="create_cvm_table",
                                        postgres_conn_id="Postgres",
                                        sql="sql/create_table.sql")

    # Operador responsável por converter os dados para o banco de dados
    opr_csv_to_postgres = PythonOperator(task_id='csv_to_postgres',
                                         python_callable=csv_to_postgres,
                                         provide_context=True)

# Execução da DAG
opr_get_cvm >> opr_create_table >> opr_csv_to_postgres
